package by.dzmitry.ariport.entity;

import java.util.ArrayList;
import java.util.Random;
import org.apache.log4j.Logger;

public class TrafficController {

	private static final Logger LOGGER = Logger.getLogger(TrafficController.class);

	private static TrafficController instance = new TrafficController();
	private ArrayList<Airport> airports = new ArrayList<Airport>();

	private TrafficController() {
		this.airports.add(new Airport("Minsk", 2, 0, 12)); // name,runways,planes,passengers
		this.airports.add(new Airport("Gavana", 2, 1, 51));
		this.airports.add(new Airport("Caracas", 1, 2, 34));
		this.airports.add(new Airport("Pyongyang", 1, 0, 0));
		this.airports.add(new Airport("Hague", 3, 1, 13));
		this.airports.add(new Airport("Donetsk", 1, 0, 201));
		this.airports.add(new Airport("Sevastopol", 3, 4, 23));
		this.airports.add(new Airport("Sochi", 1, 2, 12));
		this.airports.add(new Airport("Phnom Penh", 1, 1, 32));
	}

	public static TrafficController getInstance() {
		return instance;
	}

	public void work() {

		for (Airport airport : airports) {
			airport.start();
		}

		while (true) {
			Plane plane = takePlane(randomAirport());
			if (plane != null) {
				landingPlane(plane); // if plane flying - landing plane.
			}
		}
	}

	private void landingPlane(Plane plane) {
		int key = randomAirport(plane.getAirportId());
		while (!airports.get(key).checkFreeRunway()) {
			key = randomAirport(plane.getAirportId());
		}
		airports.get(key).addPlaneArrivalList(plane);
		plane.setAirportId(airports.get(key).getAirportId());
		plane.setFly(false);

		LOGGER.info(" @@ <- #" + plane.getPlaneId() + "[" + plane.getMaxPasseners() + ":" + plane.getPassengers().size()
				+ "] " + airports.get(key).getNameAirport() + "[passengers: "
				+ (airports.get(key).getPassengers().size() + plane.getPassengers().size()) + "][planes: "
				+ airports.get(key).getRunwaysQuantity() + ":" + (airports.get(key).getPlanes().size() + 1) + "]");

		for (Passenger passenger : plane.getPassengers()) {
			passenger.setFly(false);
			int passengerId = passenger.getPassengerId();
			Passenger pas = plane.cutPassengerById(passengerId);
			airports.get(key).putPassenger(pas);
		}
	}

	private Plane takePlane(int key) {
		Plane plane = null;
		int size = sizeAirportDispatchList(key);
		if (size != 0) {
			int randomKey = new Random().nextInt(size);
			plane = airports.get(key).cutPlaneDispatchList(randomKey);
		}
		return plane;
	}

	private int sizeAirportDispatchList(int key) {
		return airports.get(key).getDispatchList().size();

	}

	private int randomAirport() {
		return new Random().nextInt(airports.size());
	}

	private int randomAirport(int notThis) {
		int rnd = new Random().nextInt(airports.size());
		if (rnd == notThis) {
			if (rnd == 0) {
				rnd = airports.size();
			} else {
				rnd--;
			}
		}
		return rnd;
	}
}