package by.dzmitry.ariport.entity;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

import by.dzmitry.ariport.generate.GeneratorPassengerId;

public class Passenger extends Thread {
	private static final Logger LOGGER = Logger.getLogger(Passenger.class);

	private int passengerId;
	private int airportId;
	private int planeId;
	private AtomicBoolean ticket = new AtomicBoolean();
	private AtomicBoolean fly = new AtomicBoolean();
	private boolean passengerFly;
	private int countFly;

	public Passenger(int airportId) {
		this.passengerId = GeneratorPassengerId.getId();
		this.airportId = airportId;
	}

	public boolean isTicket() {
		return ticket.get();
	}

	public void setTicket(boolean value) {
		this.ticket.set(value);
	}

	public int getPassengerId() {
		return passengerId;
	}

	public void setPassengerId(int passengerId) {
		this.passengerId = passengerId;
	}

	public int getAirportId() {
		return airportId;
	}

	public void setAirportId(int airportId) {
		this.airportId = airportId;
	}

	public int getPlaneId() {
		return planeId;
	}

	public void setPlaneId(int planeId) {
		this.planeId = planeId;
	}

	public boolean isFly() {
		return fly.get();
	}

	public void setFly(boolean value) {
		this.fly.set(value);
	}

	@Override
	public void run() {
		while (true) {
			if (!isTicket() && !isFly()) {

				try {
					Thread.sleep(2000);// Passenger is buying ticket.
				} catch (InterruptedException e) {
					LOGGER.error("InterruptedException in run()");
				}
				setTicket(true);
				passengerFly = false;// Passenger brought a ticket. Happy to
										// fly!

				if (!passengerFly) {
					countFly++;
					passengerFly = true;

					try {
						Thread.sleep(2000);// Passenger is flying.
					} catch (InterruptedException e) {
						LOGGER.error("InterruptedException in run()");

					}
				}

			} else {
				try {
					Thread.sleep(2000);// Passenger sleeps in the waiting hall
										// (Only if he has a ticket).
				} catch (InterruptedException e) {
					LOGGER.error("InterruptedException in run()");

				}
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + airportId;
		result = prime * result + countFly;
		result = prime * result + ((fly == null) ? 0 : fly.hashCode());
		result = prime * result + (passengerFly ? 1231 : 1237);
		result = prime * result + passengerId;
		result = prime * result + planeId;
		result = prime * result + ((ticket == null) ? 0 : ticket.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Passenger other = (Passenger) obj;
		if (airportId != other.airportId)
			return false;
		if (countFly != other.countFly)
			return false;
		if (fly == null) {
			if (other.fly != null)
				return false;
		} else if (!fly.equals(other.fly))
			return false;
		if (passengerFly != other.passengerFly)
			return false;
		if (passengerId != other.passengerId)
			return false;
		if (planeId != other.planeId)
			return false;
		if (ticket == null) {
			if (other.ticket != null)
				return false;
		} else if (!ticket.equals(other.ticket))
			return false;
		return true;
	}
}