package by.dzmitry.ariport.entity;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;

import by.dzmitry.ariport.generate.GeneratorAirportId;

public class Airport extends Thread {

	private static final Logger LOGGER = Logger.getLogger(Airport.class);

	private int airportId;
	private String nameAirport;
	private int runwaysQuantity;
	private CopyOnWriteArrayList<Plane> planes = new CopyOnWriteArrayList<Plane>();
	private CopyOnWriteArrayList<Passenger> passengers = new CopyOnWriteArrayList<Passenger>();
	private CopyOnWriteArrayList<Plane> dispatchList = new CopyOnWriteArrayList<Plane>();
	private CopyOnWriteArrayList<Plane> arrivalList = new CopyOnWriteArrayList<Plane>();

	public Airport(String nameAirport, int runways, int planesNumber, int passengersNumber) {
		this.airportId = GeneratorAirportId.getId();
		this.nameAirport = nameAirport;
		this.runwaysQuantity = runways;

		for (int i = 0; i < planesNumber; i++) {
			int rnd = new Random().nextInt(28) + 2;
			Plane plane = new Plane(airportId, rnd); // Planes generate
			this.planes.add(plane);
		}
		for (int i = 0; i < passengersNumber; i++) {
			Passenger passenger = new Passenger(airportId); // Passengers
															// generate
			this.passengers.add(passenger);
		}
	}

	public int getRunwaysQuantity() {
		return runwaysQuantity;
	}

	public CopyOnWriteArrayList<Plane> getArrivalList() {
		return arrivalList;
	}

	public void setArrivalList(CopyOnWriteArrayList<Plane> arrivalList) {
		this.arrivalList = arrivalList;
	}

	public CopyOnWriteArrayList<Plane> getDispatchList() {
		return dispatchList;
	}

	public void setDispatchList(CopyOnWriteArrayList<Plane> planeAway) {
		this.dispatchList = planeAway;
	}

	public CopyOnWriteArrayList<Plane> getPlanes() {
		return planes;
	}

	public void setPlanes(CopyOnWriteArrayList<Plane> planes) {
		this.planes = planes;
	}

	public int getAirportId() {
		return airportId;
	}

	public void setAirportId(int aipportId) {
		this.airportId = aipportId;
	}

	public String getNameAirport() {
		return nameAirport;
	}

	public void setNameAirport(String nameAirport) {
		this.nameAirport = nameAirport;
	}

	public CopyOnWriteArrayList<Passenger> getPassengers() {
		return passengers;
	}

	public void putPassenger(Passenger passenger) {
		this.passengers.add(passenger);
	}

	public void putPlane(Plane plane) {
		this.planes.add(plane);
	}

	public Passenger cutPassenger(int key) {
		Passenger passenger = null;
		if (this.passengers.size() > key) {
			passenger = this.passengers.get(key);
			this.passengers.remove(key);
		}
		return passenger;
	}

	public Plane cutPlane(int key) {
		Plane plane = null;
		if (this.planes.size() != 0) {
			plane = this.planes.get(key);
			this.planes.remove(key);
		}
		return plane;
	}

	public Plane cutPlaneDispatchList(int key) {
		Plane plane = null;
		if (!this.dispatchList.isEmpty()) {
			plane = this.dispatchList.get(key);
			this.dispatchList.remove(key);
		}
		return plane;
	}

	public void addPlaneArrivalList(Plane plane) {
		arrivalList.add(plane);
	}

	public Plane cutPlaneArrivalList(int key) {
		Plane plane = null;
		if (!this.arrivalList.isEmpty()) {
			plane = this.arrivalList.get(key);
			this.arrivalList.remove(key);
		}
		return plane;
	}

	public void mergePassengers(ArrayList<Passenger> passengers) {
		this.passengers.addAll(passengers);
	}

	public Passenger cutPassengerById(int passengerId) {
		int i = 0;
		Passenger value = null;
		for (Passenger passenger : this.passengers) {
			if (passengerId == passenger.getPassengerId()) {
				value = passenger;
			}
			i++;
		}
		if (value != null) {
			this.passengers.remove(i);
		}
		return value;
	}

	public boolean checkFreeRunway() {
		boolean value = false;

		if (this.runwaysQuantity > planes.size()) {
			value = true;
		}
		return value;
	}

	@Override
	public void run() {
		for (Plane plane : this.planes) {
			plane.start(); // Plane thread start (for this airport)
		}

		for (Passenger passenger : this.passengers) {
			passenger.start(); // Passenger thread start (for this airport)
		}

		while (true) {
			try {
				sleep(1000);
			} catch (InterruptedException e1) {
				LOGGER.error("InterruptedException in run()");
			}
			if (!arrivalList.isEmpty()) {
				for (Plane plane : arrivalList) {
					this.planes.add(plane);
					arrivalList.remove(plane);
				}
			}
			// Add passenger into the plane and coming to fly if plane loaded
			if (this.planes != null) {
				for (Plane plane : this.planes) {
					if (plane != null) {
						if (!plane.isFill()) {
							if (!passengers.isEmpty()) {
								int randomKey = new Random().nextInt(passengers.size());
								// Add passenger with ticket into plane
								if (passengers.get(randomKey).isTicket()) {
									plane.addPassenger(cutPassenger(randomKey));
								}
							} else {
								try {
									Thread.sleep(500); // No passengers in
														// airport.
								} catch (InterruptedException e) {
									LOGGER.error("InterruptedException in run()'");
								}
							}

						} else {
							if (plane != null) {
								if (plane.getPassengers().size() != 0) {
									this.planes.remove(plane);
									this.dispatchList.add(plane);
									plane.setFly(true);
									LOGGER.info(fillSpaces(15 - getName().length()) + "@@ -> #" + plane.getPlaneId()
											+ "[" + plane.getMaxPasseners() + ":" + plane.getPassengers().size() + "] "
											+ getNameAirport() + "[passengers: " + getPassengers().size() + "][planes: "
											+ this.runwaysQuantity + ":" + getPlanes().size() + "]");
								}
							}
						}
					}
				}
			} else {
				try {
					Thread.sleep(500);// Waiting for airplanes;
				} catch (InterruptedException e) {
					LOGGER.error("InterruptedException in run()");

				}
			}
		}
	}

	private String fillSpaces(int len) {
		String spaces = "                                                                              ";
		return spaces.substring(0, len);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + airportId;
		result = prime * result + ((arrivalList == null) ? 0 : arrivalList.hashCode());
		result = prime * result + ((dispatchList == null) ? 0 : dispatchList.hashCode());
		result = prime * result + ((nameAirport == null) ? 0 : nameAirport.hashCode());
		result = prime * result + ((passengers == null) ? 0 : passengers.hashCode());
		result = prime * result + ((planes == null) ? 0 : planes.hashCode());
		result = prime * result + runwaysQuantity;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Airport other = (Airport) obj;
		if (airportId != other.airportId)
			return false;
		if (arrivalList == null) {
			if (other.arrivalList != null)
				return false;
		} else if (!arrivalList.equals(other.arrivalList))
			return false;
		if (dispatchList == null) {
			if (other.dispatchList != null)
				return false;
		} else if (!dispatchList.equals(other.dispatchList))
			return false;
		if (nameAirport == null) {
			if (other.nameAirport != null)
				return false;
		} else if (!nameAirport.equals(other.nameAirport))
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		if (planes == null) {
			if (other.planes != null)
				return false;
		} else if (!planes.equals(other.planes))
			return false;
		if (runwaysQuantity != other.runwaysQuantity)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Airport [airportId=" + airportId + ", nameAirport=" + nameAirport + ", runwaysQuantity="
				+ runwaysQuantity + ", planes=" + planes + ", passengers=" + passengers + ", dispatchList="
				+ dispatchList + ", arrivalList=" + arrivalList + "]";
	}

}