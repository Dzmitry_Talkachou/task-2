package by.dzmitry.ariport.run;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

import by.dzmitry.ariport.entity.TrafficController;

public class Runner {
	static {
		new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
	}
	public static void main(String[] args) {
		TrafficController tc = TrafficController.getInstance();
		tc.work();
	}
}
