package by.dzmitry.ariport.entity;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

import by.dzmitry.ariport.generate.GeneratorPlaneId;

public class Plane extends Thread {

	private static final Logger LOGGER = Logger.getLogger(Plane.class);

	private int planeId;
	private int airportId;
	private int maxPasseners;
	private AtomicBoolean fill = new AtomicBoolean();
	private AtomicBoolean fly = new AtomicBoolean();
	private CopyOnWriteArrayList<Passenger> passengers = new CopyOnWriteArrayList<Passenger>();

	public Plane(int maxPasseners, int airportId) {
		this.planeId = GeneratorPlaneId.getId();
		this.maxPasseners = maxPasseners;
		this.airportId = airportId;
	}

	public int getMaxPasseners() {
		return maxPasseners;
	}

	public CopyOnWriteArrayList<Passenger> getPassengers() {
		return passengers;
	}

	public boolean isFill() {
		return fill.get();
	}

	public void setFill(boolean value) {
		this.fill.set(value);
	}

	public boolean isFly() {
		return fly.get();
	}

	public void setFly(boolean value) {
		this.fly.set(value);
	}

	public int getPlaneId() {
		return planeId;
	}

	public void setPlaneId(int planeId) {
		this.planeId = planeId;
	}

	public int getAirportId() {
		return airportId;
	}

	public void setAirportId(int airportId) {
		this.airportId = airportId;
	}

	public void setPassengers(CopyOnWriteArrayList<Passenger> passengers) {
		this.passengers = passengers;
	}

	public Passenger cutPassenger(int key) {
		Passenger passenger = null;
		if (this.passengers.size() != 0) {
			passenger = this.passengers.get(key);
			this.passengers.remove(key);
		}
		return passenger;
	}

	public int passengersSize() {
		return this.passengers.size();
	}

	public void addPassenger(Passenger passenger) {
		this.passengers.add(passenger);
	}

	public Passenger cutPassengerById(int passengerId) {
		int i = 0;
		Passenger value = null;
		if (!passengers.isEmpty()) {
			for (Passenger passenger : this.passengers) {
				if (passengerId == passenger.getPassengerId()) {
					value = passenger;
					this.passengers.remove(i);
				}

				i++;
			}

		} else {
			LOGGER.info("Passengers is empty, return null.");
		}
		return value;
	}

	@Override
	public void run() {
		while (true) {
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				LOGGER.error("InterruptedException in run()");
			}

			if (this.maxPasseners == this.passengers.size()) {
				if (!isFill()) {
					setFill(true); // plane loaded and ready to fight
					for (Passenger passenger : passengers) {
						passenger.setTicket(false);// pick up all the tickets.
						passenger.setFly(true);
					}
					setFly(true);
				}

				try {
					Thread.sleep(1000);// Waiting to start.
				} catch (InterruptedException e) {
					LOGGER.error("InterruptedException in run()");
				}

			} else {
				if (!isFly() && isFill()) {
					setFill(false);
					try {
						Thread.sleep(1000);// Wait after landing.
					} catch (InterruptedException e) {
						LOGGER.error("InterruptedException in run()");
					}
				}
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + airportId;
		result = prime * result + ((fill == null) ? 0 : fill.hashCode());
		result = prime * result + ((fly == null) ? 0 : fly.hashCode());
		result = prime * result + maxPasseners;
		result = prime * result + ((passengers == null) ? 0 : passengers.hashCode());
		result = prime * result + planeId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plane other = (Plane) obj;
		if (airportId != other.airportId)
			return false;
		if (fill == null) {
			if (other.fill != null)
				return false;
		} else if (!fill.equals(other.fill))
			return false;
		if (fly == null) {
			if (other.fly != null)
				return false;
		} else if (!fly.equals(other.fly))
			return false;
		if (maxPasseners != other.maxPasseners)
			return false;
		if (passengers == null) {
			if (other.passengers != null)
				return false;
		} else if (!passengers.equals(other.passengers))
			return false;
		if (planeId != other.planeId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Plane [planeId=" + planeId + ", airportId=" + airportId + ", maxPasseners=" + maxPasseners + ", fill="
				+ fill + ", fly=" + fly + ", passengers=" + passengers + "]";
	}

}
